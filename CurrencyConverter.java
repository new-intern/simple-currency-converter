import java.util.Scanner;

public class CurrencyConverter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Currency Converter");
        System.out.println("====================");
        while (true) {
                System.out.print("Enter amount in INR: ");
                double usdAmount = scanner.nextDouble();
                System.out.print("Choose currency to convert to (EUR, GBP, USD): ");
                String targetCurrency = scanner.next().toUpperCase();
                double exchangeRate = getExchangeRate(targetCurrency);
                if (exchangeRate == -1) {
                        System.out.println("Invalid currency selection. Please choose EUR, GBP, or USD.");
                        continue;
                        }
                double convertedAmount = usdAmount * exchangeRate;
                System.out.printf("%.2f USD is %.2f %s%n", usdAmount, convertedAmount, targetCurrency);
                System.out.print("Do you want to convert another amount? (yes/no): ");
                String repeat = scanner.next().toLowerCase();
                if (!repeat.equals("yes")) {
                        System.out.println("Goodbye!");
                        break;
                        }
                }
                scanner.close();
        }
private static double getExchangeRate(String targetCurrency) {
        switch (targetCurrency) {
                case "EUR":
                        return 0.011;
                case "GBP":
                        return 0.095;
                case "USD":
                        return 0.012;
                default:
                        return -1;
        }
}
}
                                                                                                                                                                                                                                                                                                                                                                                                                            
